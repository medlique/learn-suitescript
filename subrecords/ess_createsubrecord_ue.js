define(["N/config"], function (config) {

    /**
     * Demonstrates creating Subrecords on the server-side
     *
     * @exports ess-createsubrecord-ue
     *
     * @copyright 2018 Stoic Software, LLC
     * @author Eric T Grubaugh <eric@stoic.software>
     *
     * @NApiVersion 2.x
     * @NScriptType UserEventScript
     */
    var exports = {};


    /**
     * <code>beforeSubmit</code> event handle
     *
     * @gov XXX
     *
     * @param context
     *        {Object}
     * @param context.newRecord
     *        {record} The new record being submitted
     * @param context.oldRecord
     *        {record} The old record before it was modified
     * @param context.type
     *        {UserEventType} The action type that triggered this event
     *
     * @return {void}
     *
     * @static
     * @function beforeSubmit
     */
    function beforeSubmit(context) {
        log.audit({title: "Adding company address..."});

        var companyAddress = getCompanyAddress();
        addCompanyAddress(context.newRecord, companyAddress);

        log.audit({title: "Company address added."});
    }

    function getCompanyAddress() {
        log.audit({title: "Retrieving company address..."});

        var companyInfo = config.load({
            type: config.Type.COMPANY_INFORMATION
        });

        var companyAddress = companyInfo.getSubrecord({fieldId: "mainaddress"});

        return {
            addr1: companyAddress.getValue({fieldId: "addr1"}),
            addr2: companyAddress.getValue({fieldId: "addr2"}),
            addr3: companyAddress.getValue({fieldId: "addr3"}),
            city: companyAddress.getValue({fieldId: "city"}),
            country: companyAddress.getValue({fieldId: "country"}),
            state: companyAddress.getValue({fieldId: "state"}),
            zip: companyAddress.getValue({fieldId: "zip"})
        };
    }

    function addCompanyAddress(rec, address) {
        log.audit({title: "Creating new subrecord..."});

        rec.insertLine({sublistId: "addressbook", line: 0});

        var newAddress = rec.getSublistSubrecord({
            sublistId: "addressbook",
            fieldId: "addressbookaddress",
            line: 0
        });

        newAddress.setValue({fieldId: "country", value: address.country});
        newAddress.setValue({fieldId: "state", value: address.state});
        newAddress.setValue({fieldId: "city", value: address.city});
        newAddress.setValue({fieldId: "zip", value: address.zip});
        newAddress.setValue({fieldId: "addr1", value: address.addr1});
        newAddress.setValue({fieldId: "addr2", value: address.addr2});
        newAddress.setValue({fieldId: "addr3", value: address.addr3});
    }

    exports.beforeSubmit = beforeSubmit;
    return exports;
});
