/**
 * An example of nlapiLookupField to retrieve multiple fields from a related record
 */

// Get the Sales Rep record ID
var repId = nlapiGetFieldValue("salesrep");

// Retrieve multiple fields from the associated Sales Rep
var repData = nlapiLookupField("employee", repId, ["email", "firstname"]);

console.log(repData);
console.log(repData.firstname + "'s email address is " + repData.email);