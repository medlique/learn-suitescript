define(["N/record", "N/redirect"], function (r, redirect) {

    /**
     * Redirects to a new record
     *
     * @exports redirect-new
     *
     * @copyright 2018 Stoic Software, LLC
     * @author Eric T Grubaugh <eric@stoic.software>
     *
     * @NApiVersion 2.x
     * @NModuleScope SameAccount
     * @NScriptType UserEventScript
     * @appliedtorecord employee
     */
    var exports = {};

    const FORM_ID = 231;

    /**
     * <code>beforeLoad</code> event handler
     * for details.
     *
     * @gov 0
     *
     * @param context
     *        {Object}
     * @param context.newRecord
     *        {record} The new record being loaded
     * @param context.type
     *        {UserEventType} The action type that triggered this event
     * @param context.form
     *        {form} The current UI form
     *
     * @return {void}
     *
     * @static
     * @function beforeLoad
     */
    function beforeLoad(context) {

        if (!needsRedirect(context)) {
            return;
        }

        redirect.toRecord({
            type: r.Type.EMPLOYEE,
            id: '', // Empty string here should redirect to new record; null/undefined do not work,
            parameters: {cf: FORM_ID}
        })
    }

    function needsRedirect(context) {
        return ([context.UserEventType.VIEW, context.UserEventType.DELETE].indexOf(context.type) !== -1);
    }

    exports.beforeLoad = beforeLoad;
    return exports;
});
