define(["N/http", "N/error"], function (http, error) {

    /**
     * An example using the Event Router pattern for a Suitelet handling
     * HTTP requests
     *
     * @exports good-example/sl
     *
     * @copyright 2017 Stoic Software, LLC
     * @author Eric T Grubaugh <eric@stoic.software>
     *
     * @NApiVersion 2.x
     * @NModuleScope SameAccount
     * @NScriptType Suitelet
     */
    var exports = {};

    /**
     * <code>onRequest</code> event handler
     *
     * @governance XXX
     *
     * @param context
     *        {Object}
     * @param context.request
     *        {ServerRequest} The incoming request object
     * @param context.response
     *        {ServerResponse} The outgoing response object
     *
     * @return {void}
     *
     * @static
     * @function onRequest
     */
    function onRequest(context) {
        var eventRouter = {};
        eventRouter[http.Method.GET] = handleGet;
        eventRouter[http.Method.POST] = handlePost;

        eventRouter[context.request.method] ?
            eventRouter[context.request.method](context) :
            handleError(context);
    }

    function handleGet(context) {
        // Do a search

        // Render a form

        // Write to response
        context.response.writePage(form);
    }

    function handlePost(context) {
        // Parse form data

        // Process form data

        // Send email

        // Write to response
    }

    function handleError(context) {
        // Email admin

        throw error.create({
            name: "SSS_UNSUPPORTED_REQUEST_TYPE",
            message: "Suitelet only supports GET and POST",
            notifyOff: true
        });
    }

    exports.onRequest = onRequest;
    return exports;
});
